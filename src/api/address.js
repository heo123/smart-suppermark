import request from '@/utils/request'

// 获取地址列表
export const getAddressList = () => {
  return request.get('/address/list')
}
// 添加默认地址
export const addAddressAPI=(name,phone,region,detail)=>{
  return request.post('/address/add',{
    name,
    phone,
    region,
    detail
  })
}
